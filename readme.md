
# indoor sensor board

## schematic


![schematic rev3](pics/schematic-rev3.svg)

## pcb


rev 3 - front silkscreen

![pcb rev3](pics/pcb-rev3-front.svg)

rev 3 - back silkscreen

![pcb rev3](pics/pcb-rev3-back.svg)

rev 3 both

![pcb rev3](pics/pcb-rev-3.png)

front pcb

![pcb](pics/pcb-back.png)

back pcb
![pcb](pics/pcb-front.png)

## renders

rev 3 2.13 inch eink we act

![pcb](pics/3d-front-2-13.png)

![pcb](pics/3d-back-2-13.png)


rev 3 1.54 inch eink we act

![pcb](pics/3d-front-1-54.png)

![pcb](pics/3d-back-1-54.png)

3d pcb view front, not components

![pcb](pics/pcb-3d-front-no.png)


3d pcb rev 2 view front

![pcp](pics/pcb-3d-front.png)

3d pcb rev 2 view back, not components

![pcb 3d](pics/pcb-3d-back-no.png)

3d pcb rev 2 view back

![pcb](pics/pcb-3d-back.png)


## bom

- esp32-c3-fh4: weact used (rev1 and 2 only)
- esp32-c3 super mini, rev 3 only
- temt 6000 light sensor rev 2 and 3 only
- dil 24 way wide socket (rev 1 and 2 only)
- bme280 sensor 3.3/5V, solder the right jumper
- bme680 sensor rev 3 only spi connection
- sgp30 and sgp40 air quality sensors
- 2.13'' eink ssd1680 screen, weact footprint or 1.54 eink ssd1681 screen we act
- lego plate 6x12
